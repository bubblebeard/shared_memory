/* 5.5. Винни-Пух и пчелы. Заданное количество пчел добывают мед равными порциями,
 *  задерживаясь в пути на случайное время. Винни-Пух потребляет мед порциями 
 * заданной величины за заданное время и столько же времени может прожить
 *  без питания. Работа каждой пчелы реализуется в порожденном процессе.*/

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <signal.h>

#define PORTION 5
#define HONEY 100
#define LOOP 1
#define REQ 1
#define TERM -1

union semun {
    int val; /* значение для SETVAL */
    struct semid_ds *buf; /* буферы для  IPC_STAT, IPC_SET */
    unsigned short *array; /* массивы для GETALL, SETALL */
    /* часть, особенная для Linux: */
    struct seminfo *__buf; /* буфер для IPC_INFO */
};

struct sembuf lock_res = {0, -1, 0};
struct sembuf rel_res = {0, 1, 0};

int writer(int, int*, int);
int reader(int, int*, int, int, int, int);

int main(int argc, char** argv) {
    pid_t childpid;
    union semun arg;
    int bees = atoi(argv[1]); //кол-во пчёл
    int time = atoi(argv[2]); //интервал через который винни ест
    int meal = atoi(argv[3]); //сколько винни съедает за раз
    int semid, *p, shm;
    semid = semget(IPC_PRIVATE, 1, 0666 | IPC_CREAT);
    printf("semid - %d\n",semid);
    fflush(stdout);
    shm = shmget(IPC_PRIVATE, sizeof (int), 0666 | IPC_CREAT);
    printf("shared memory ID - %d\n",shm);
    fflush(stdout);
    p = shmat(shm, NULL, 0);
    *p = HONEY;
    arg.val = 1;
    semctl(semid, 0, SETVAL, arg);
    for (int i = 0; i < bees; i++) {
        if ((childpid = fork()) == -1) {
            perror("main: fork error");
            exit(1);
        }
        if (childpid == 0) {
            writer(semid, p, shm, i);
            exit(0);
        }
    }
    reader(semid, p, shm, bees, meal, time);
    return (0);
}

int writer(int semid, int* ptr, int index) {
    while (LOOP) {
        if (semop(semid, &lock_res, 1) == -1) {
            perror("semop: lock_res\n");
        }
        printf("bee%d: shm locked\n", index);
        printf("bee%d: %d honey left\n", index, *ptr);
        fflush(stdout);
        *ptr += PORTION;
        semop(semid, &rel_res, 1);
        printf("bee%d: shm unlocked\n", index);
        fflush(stdout);
        sleep(rand() % 5);
    }
    return (EXIT_FAILURE);
}

int reader(int semid, int* ptr, int shm, int bees, int meal, int time) {

    while (LOOP) {
        if (semop(semid, &lock_res, 1) == -1) {
            perror("semop: lock_res\n");
        }
        printf("Winnie: shm locked\n");
        fflush(stdout);
        if (*ptr < meal) {
            printf("Winnie: not enough honey\n");
            semop(semid, &rel_res, 1);
            printf("Winnie: shm unlocked\n");
            fflush(stdout);
            sleep(time);
            if (semop(semid, &lock_res, 1) == -1) {
                perror("semop: lock_res\n");
            }
            printf("Winnie: shm locked\n");
            fflush(stdout);
            if (*ptr < meal) {
                printf("Winnie died. Rest In Pepperonis\n");
                fflush(stdout);
                signal(SIGQUIT, SIG_IGN);
                kill(-getpid(), SIGQUIT);
                semop(semid, &rel_res, 1);
                printf("Winnie: shm unlocked\n");
                fflush(stdout);
                shmctl(shm, IPC_RMID, NULL);
                semctl(semid, 0, IPC_RMID);

                return (EXIT_SUCCESS);
            } else {
                *ptr -= meal;
                printf("Winnie eat %d honey. %d honey left\n", meal, *ptr);
                semop(semid, &rel_res, 1);
                printf("Winnie: shm unlocked\n");
                fflush(stdout);
            }
        } else {
            *ptr -= meal;
            printf("Winnie eat %d honey. %d honey left\n", meal, *ptr);
            semop(semid, &rel_res, 1);
            printf("Winnie: shm unlocked\n");
            fflush(stdout);
            sleep(time);
        }
    }

    return (EXIT_SUCCESS);
}

